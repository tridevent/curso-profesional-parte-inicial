﻿using UnityEngine;
using System.Collections;

public class Ejercicio3 : MonoBehaviour {
	public AudioClip sonido;
	public GameObject[] puerta;
	public int aleatorio;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider parametro){
	if(parametro.gameObject.tag=="plataforma")
		{
			for(int i=0;i<puerta.Length;i++)
			{
				puerta[i].GetComponent<Collider>().isTrigger=false;
			}
			aleatorio= Random.Range(0,puerta.Length);
			puerta[aleatorio].GetComponent<Collider>().isTrigger=true;
			AudioSource.PlayClipAtPoint(sonido,transform.position);
		}
	}
}
